/**
 * Overrides the standard OmniFund Payment Processor
 *
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

// @ts-ignore
import {OmniFund} from '/.bundle/251911/OmniFund';

export let process = () => {
    let omniFund: OmniFund = OmniFund.getInstance('test');
};